﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net.Security;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace ClienteConsoladorReal
{
    class Program
    {
       //Variables Globlales Inicio
        private static int _hostPort = 2000;
        private static string _hostName = "localhost";
        private static string ServerCertificateName ="MySslSocketCertificate";
        //Variables Globales Final

        [STAThread]
        static void Main(string[] args)
        {

            var clientCertificate = getServerCert();
            var clientCertificateCollection = new X509CertificateCollection(new X509Certificate[] { clientCertificate });

            using (var client = new TcpClient(_hostName, _hostPort))
            using (var sslStream = new SslStream(client.GetStream(), false, ValidateCertificate))
            {
                sslStream.AuthenticateAsClient(ServerCertificateName, clientCertificateCollection, SslProtocols.Tls12, false);

                var outputMessage = "Brindame tus caracterisicas servidor";
                var outputBuffer = Encoding.UTF8.GetBytes(outputMessage);
                sslStream.Write(outputBuffer);
                Console.WriteLine("Solucitud: {0}", outputMessage);

                var inputBuffer = new byte[4096];
                var inputBytes = 0;
                while (inputBytes == 0)
                {
                    inputBytes = client.GetStream().Read(inputBuffer, 0, inputBuffer.Length);
                }
                var respuestaServer = Encoding.UTF8.GetString(inputBuffer,
                   0, inputBytes);
                Console.WriteLine("Respuesta del Servidor: {0}", ("\n"+respuestaServer));
            }



            //string mensaje = "Hola... <FIN>";
            //string respuesta;
            //Byte[] SendBytes = Encoding.ASCII.GetBytes(mensaje);
            //Byte[] RecvBytes = new Byte[256];
            //int bytes;
            //// Cliente TCP
            //TcpClient client = new TcpClient();
            //// Conexión
            //try
            //{
            //    client.Connect(Dns.GetHostName(), 11000);
            //    client.GetStream().Write(SendBytes, 0, SendBytes.Length);
            //    bytes = client.GetStream().Read(RecvBytes, 0, RecvBytes.Length);
            //    respuesta = Encoding.ASCII.GetString(RecvBytes, 0, bytes);
            //    while (bytes > 0)
            //    {
            //        bytes = client.GetStream().Read(RecvBytes, 0, RecvBytes.Length);
            //        respuesta += Encoding.ASCII.GetString(RecvBytes, 0, bytes);
            //    }
            //    Console.WriteLine(respuesta);
            //}
            //catch (Exception error)
            //{
            //    Console.WriteLine("ERROR - " + error);
            //}
        }







        //Validar Certificado inicio
        static bool ValidateCertificate(Object sender,X509Certificate certificate, X509Chain chain,SslPolicyErrors sslPolicyErrors)
        {
            if (sslPolicyErrors == SslPolicyErrors.None)
            { return true; }
            // ignore chain errors as where self signed
            if (sslPolicyErrors ==
               SslPolicyErrors.RemoteCertificateChainErrors)
            { return true; }
            return false;
        }
        //validar Certificado Final

        //Obtener certificado del servidor inicio
        private static X509Certificate getServerCert()
        {
            X509Store store = new X509Store(StoreName.My,StoreLocation.CurrentUser);
            store.Open(OpenFlags.ReadOnly);

            X509Certificate2 foundCertificate = null;
            foreach (X509Certificate2 currentCertificate in store.Certificates)
            {
                if (currentCertificate.IssuerName.Name
                   != null && currentCertificate.IssuerName.
                   Name.Equals("CN=MySslSocketCertificate"))
                {
                    foundCertificate = currentCertificate;
                    break;
                }
            }


            return foundCertificate;
        }
        //Obtener certificado del servidor final

    }
}
