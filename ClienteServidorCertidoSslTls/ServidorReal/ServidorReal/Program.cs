﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Management;
using Microsoft.Win32;
using System.Net.Security;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Text;


namespace ServidorReal
{

    class Program
    {
        //Variables Globales inicio
        private static int _listeningPort = 2000;
        //Variables Globales Fin

        [STAThread]
        static void Main(string[] args)
        {
            Console.WriteLine("Esperando Conexion...");
            IPHostEntry ipHostInfo = Dns.Resolve(Dns.GetHostName());
            IPAddress ipAddress = ipHostInfo.AddressList[0];
            var serverCertificate = getServerCert();
            var listener = new TcpListener(IPAddress.Any, _listeningPort);
            listener.Start();

            while (true)
            {
                using (var client = listener.AcceptTcpClient())
                using (var sslStream = new SslStream(client.GetStream(), false, ValidateCertificate))
                {
                    //Componentes servidor Inicio
                    ManagementScope scope = new ManagementScope("\\root\\cimv2");
                    ObjectQuery query = new ObjectQuery("SELECT * FROM Win32_LogicalDisk where drivetype=3");
                    ManagementObjectSearcher mos = new ManagementObjectSearcher(scope, query);
                    string descripcion = "Estado: Corriendo" + " \n ";
                    String path = "HARDWARE\\DESCRIPTION\\System\\CentralProcessor\\0";
                    RegistryKey key = Registry.LocalMachine.OpenSubKey(path);
                    String processor = key.GetValue("ProcessorNameString").ToString();
                    descripcion = descripcion + "Procesador=" + processor + " \n ";
                    foreach (ManagementObject item in mos.Get())
                    {
                        long hddSizeBytes = Int64.Parse(item["Size"].ToString());
                        double hddSizeGBytes = hddSizeBytes / 1024 / 1024 / 1024;
                        descripcion = descripcion + "Tamaño =" + hddSizeGBytes + "Gb" + " \n ";

                    }
                    //Componentes servidor Final


                    sslStream.AuthenticateAsServer(serverCertificate, true, SslProtocols.Tls12, false);
                    var inputBuffer = new byte[4096];
                    var inputBytes = 0;
                    while (inputBytes == 0)
                    {
                        inputBytes = sslStream.Read(inputBuffer, 0,inputBuffer.Length);
                    }
                    var inputMessage = Encoding.UTF8.GetString(inputBuffer,
                       0, inputBytes);
                    Console.WriteLine("Peticion: {0}", inputMessage+ " \n Fecha y Hora de la Solicitud: " + DateTime.Now.Date.ToString());

                    var sendMessage = System.Text.Encoding.ASCII.GetBytes(descripcion);
                    client.GetStream().Write(sendMessage, 0, sendMessage.Length);
                    client.Close();
                    

                }
            }




            




            //TcpListener listenerw = new TcpListener(11000);
            //listenerw.Start();
            //Console.WriteLine("Waiting for a connection...");
            //TcpClient clientw = listenerw.AcceptTcpClient();
            //byte[] buffer = new byte[1024];
            //int    count;
            //String data = null;
            //do	
            //{
            //	count = clientw.GetStream().Read(buffer,0,1024);
            //	data += System.Text.Encoding.ASCII.GetString(buffer,0,count);
            //} while ( data.IndexOf("<FIN>") == -1 );
            //// Echo
            //Console.WriteLine( "Text received : {0}", data+""+descripcion);
            //byte[] msg = System.Text.Encoding.ASCII.GetBytes(data+""+descripcion);
            //clientw.GetStream().Write(msg,0,msg.Length);
            //clientw.Close();
            //listenerw.Stop();
        }


        //Validar Certificado Inicio
        static bool ValidateCertificate(Object sender,X509Certificate certificate, X509Chain chain,SslPolicyErrors sslPolicyErrors)
        {
            return true;
            if (sslPolicyErrors == SslPolicyErrors.None)
            { return true; }
            // we don't have a proper certificate tree
            if (sslPolicyErrors ==
                  SslPolicyErrors.RemoteCertificateChainErrors)
            { return true; }
            return false;
        }
        //Validar Certificado Final

        //Obtener Certificados Inicio
        private static X509Certificate getServerCert()
        {
            X509Store store = new X509Store(StoreName.My,StoreLocation.CurrentUser);
            store.Open(OpenFlags.ReadOnly);
            X509Certificate2 foundCertificate = null;
            foreach (X509Certificate2 currentCertificate in store.Certificates)
            {
                if (currentCertificate.IssuerName.Name
                   != null && currentCertificate.IssuerName.
                   Name.Equals("CN=MySslSocketCertificate"))
                {
                    foundCertificate = currentCertificate;
                    break;
                }
            }
            return foundCertificate;
        }
        //Obtener Certificados Final
    }
}
